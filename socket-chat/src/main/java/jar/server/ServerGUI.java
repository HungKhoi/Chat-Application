package jar.server;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import mdlaf.MaterialLookAndFeel;

public class ServerGUI {
    static {
        try {
            UIManager.setLookAndFeel(new MaterialLookAndFeel());
            UIManager.put("Button.mouseHoverEnable", true);
            JFrame.setDefaultLookAndFeelDecorated(false);
            
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }
    private JFrame serverFrame = null;
    public static JTextField txtIP, txtPort;
    public static JTextArea txtServerMessage;
    private Server server = null;
    public ServerGUI() {
        initUI();
    }

    public void initUI() {
        initFrame();
        initLabel();
        initTextBox();
        initButton();
    }

    public void initFrame() { 
        serverFrame = new JFrame();
        serverFrame.setTitle("Server");
        serverFrame.setResizable(false);
        serverFrame.setSize(550, 412);
        serverFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        serverFrame.getContentPane().setLayout(null);
        serverFrame.setLocationRelativeTo(null);
    }

    public void initLabel() {
        JLabel ipLabel = new JLabel("IP: ");
        ipLabel.setBounds(5, 25, 40, 16);
        serverFrame.getContentPane().add(ipLabel);
        JLabel portLabel = new JLabel("Port: ");
        portLabel.setBounds(285, 25, 50, 16);
        serverFrame.getContentPane().add(portLabel);
    }

    public void initTextBox() {
        txtIP = new JTextField("localhost");
        txtIP.setEditable(false);
        txtIP.setBounds(55, 19, 210, 28);
        serverFrame.getContentPane().add(txtIP);
        txtPort = new JTextField("8080");
        txtPort.setEditable(false);
        txtPort.setBounds(336, 19, 208, 28);
        serverFrame.getContentPane().add(txtPort);
        txtServerMessage = new JTextArea();
        txtServerMessage.setText("");
        txtServerMessage.setEditable(false);
        txtServerMessage.setBounds(5, 100, 540, 270);
        txtServerMessage.setFont(new Font("Hacker", Font.BOLD, 10));
        serverFrame.getContentPane().add(txtServerMessage);
    }

    public void initButton() {
        JButton btnStart = new JButton("Bắt đầu");
        btnStart.setBounds(5, 60, 260, 29);
        btnStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    server = new Server(txtIP.getText(), Integer.parseInt(txtPort.getText()));
                }
                catch (Exception exception) {
                    JOptionPane.showConfirmDialog(serverFrame, "Server Error", "Lỗi", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        JButton btnStop = new JButton("Dừng");
        btnStop.setBounds(285, 60, 260, 29);
        btnStop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    if (server != null) {
                        server.exit();
                        txtServerMessage.append("STOP SERVER\n");
                    }
                    else {
                        JOptionPane.showConfirmDialog(serverFrame, "Server Not Start", "Lỗi", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                    }
                }
                catch (Exception exception) {
                    exception.printStackTrace();
                    txtServerMessage.append("STOP SERVER\n");
                }
            }
        });
        serverFrame.getContentPane().add(btnStart);
        serverFrame.getContentPane().add(btnStop);
    }
    public static void main(String[] args )
    {
        EventQueue.invokeLater(new Runnable() {
                @Override
                public void run(){
                    ServerGUI serverScreen = new ServerGUI();
                    serverScreen.serverFrame.setVisible(true);
                }
            });
    }
}