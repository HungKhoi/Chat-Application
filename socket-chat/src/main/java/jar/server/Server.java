package jar.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import jar.protocol.Tags;
import jar.protocol.Decode;
import jar.protocol.Encode;
import jar.protocol.Peer;

public class Server {
    private Server() {}
    private String hostName = "";
    private int port = 0;
    private ServerSocket server = null;
    private boolean isStop = false;
    private ArrayList<Peer> peer_List = null;
    public Server(String host, int port) throws Exception{
        this.hostName = host;
        this.port = port;
        this.peer_List = new ArrayList<Peer>();
        server = new ServerSocket(this.port, 50, InetAddress.getByName(this.hostName));
        ServerGUI.txtServerMessage.append("SERVER START\n");
        (new WaitForConnect()).start();
    }
    class WaitForConnect extends Thread {
        Socket connection;
        ObjectInputStream request;
        ObjectOutputStream sender;
        @Override
        public void run() {
            super.run();
            while (!isStop) {
                try {
                    connection = server.accept();
                    request = new ObjectInputStream(connection.getInputStream());
                    String receivedMessage = (String) request.readObject();
                    ArrayList<String> acc_Info = Decode.getAccountInformation(receivedMessage);
                    String sendMessage = "";
                    if (acc_Info != null) {
                        if (searchIndexPeer(acc_Info.get(0)) == -1) {
                            Peer new_Peer = new Peer();
                            new_Peer.setPeer(acc_Info.get(0), connection.getInetAddress().getHostAddress(), Integer.parseInt(acc_Info.get(1)));
                            peer_List.add(new_Peer);
                            ServerGUI.txtServerMessage.append(receivedMessage + "\n");
                            sendMessage = Encode.genPeerListMessage(peer_List);
                        }
                        else {
                            sendMessage = Tags.SESSION_DENY_TAG;
                        }
                    }
                    else {
                        String name = Decode.getDiedAccount(receivedMessage);
                        if (name != null) {
                            peer_List.remove(searchIndexPeer(name));
                            sendMessage = Encode.genPeerListMessage(peer_List);
                        }
                        else {
                            sendMessage = Encode.genPeerListMessage(peer_List);
                        }
                    }
                    sender = new ObjectOutputStream(connection.getOutputStream());
                    sender.writeObject(sendMessage);
                    sender.flush();
                    sender.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
                server.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void exit() {
        try {
            isStop = true;
            server.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int searchIndexPeer(String name) {
        int index = -1;
        for (int i = 0; i < peer_List.size(); i++) {
            if (name.equals(peer_List.get(i).getName())) {
                index = i;
                return index;
            }
        }
        return index;
    }
}