package jar.client;

import java.awt.EventQueue;
import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import mdlaf.MaterialLookAndFeel;
import jar.protocol.Decode;
import jar.protocol.Peer;

import java.awt.event.ActionListener;
import java.net.Socket;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.util.ArrayList;

public class MainGUI {

    static {
        try {
            UIManager.setLookAndFeel(new MaterialLookAndFeel());
            UIManager.put("Button.mouseHoverEnable", true);
            JFrame.setDefaultLookAndFeelDecorated(false);
            
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }

    private String server_IP = "";
    private int server_Port = 8080;
    private int peer_Port = 0;
    private String user_Name = "";
    private ArrayList<Peer> current_Peer_List = null;
    private String workingDir = System.getProperty("user.dir");
    private JFrame mainFrame = null;
    private JTextField txtUsername;
    private boolean isStop = false;
    private Menu menuClient = null;
    private ServerClient server = null;
    private JList<UserItem> listUserItem;
    private String peer_List = "";
    private JScrollPane temp = null;
    private String selected_Friend_Name = "";

    public MainGUI(String server_ip, int server_port, int peer_port, String user_name, String message) {
        this.server_IP = server_ip;
        this.server_Port = server_port;
        this.peer_Port = peer_port;
        this.user_Name = user_name;
        try {
            this.menuClient = new Menu(server_IP, server_Port, user_Name);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        initUI();
        displayFriendList(message);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1500);
                    updateFriendList();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        server = new ServerClient(user_Name, peer_port);
    }

    public void show() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run(){
                mainFrame.setVisible(true);
            }
        });
    }
    private void initUI() {
        initFrame();
        initLabel();
        initTextBox();
        initButton();
    }

    private void initFrame() {
        mainFrame = new JFrame();
        mainFrame.setTitle("Main");
        mainFrame.setResizable(false);
        mainFrame.setSize(340, 550);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.getContentPane().setLayout(null);
        mainFrame.setLocationRelativeTo(null);
    }

    private void initLabel() {
        JLabel lbIcon = new JLabel();
        lbIcon.setIcon(new ImageIcon(workingDir + "/src/main/java/jar/images/rsz_generic_image.png"));
        lbIcon.setBounds(5, 5, 80, 80);
        mainFrame.getContentPane().add(lbIcon);
        JLabel userLabel = new JLabel("User name: ");
        userLabel.setBounds(90, 37, 100, 16);
        mainFrame.getContentPane().add(userLabel);
    }

    private void initTextBox() {
		txtUsername = new JTextField(this.user_Name);
		txtUsername.setEditable(false);
		txtUsername.setColumns(10);
		txtUsername.setBounds(180, 34, 150, 28);
		mainFrame.getContentPane().add(txtUsername);
    }
    
    private void displayListUserItem() {
        listUserItem = createListUserItems();
        temp = new JScrollPane(listUserItem);
        temp.setBounds(10, 100, 320, 372);
        mainFrame.getContentPane().add(temp);
    }

    private JList<UserItem> createListUserItems() {
        DefaultListModel<UserItem> model = new DefaultListModel<>();
        if (current_Peer_List == null) return null;
        for (int i = 0; i < current_Peer_List.size(); i++) {
            if (!user_Name.equals(current_Peer_List.get(i).getName())) {
                model.addElement(new UserItem(current_Peer_List.get(i).getName(), workingDir + "/src/main/java/jar/images/rsz1_generic_image.png"));
            }
        }
        final JList<UserItem> list = new JList<UserItem>(model);
        list.setCellRenderer(new UserItemRenderer());
        list.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent arg0) {
                if (!arg0.getValueIsAdjusting()) {
                    selected_Friend_Name = new String(list.getSelectedValue().getName());
                }
            }
        });
        return list;
    }

    private void initButton() {
        JButton btnChat = new JButton("Chat");
        btnChat.setBounds(10, 478, 113, 29);
        btnChat.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (selected_Friend_Name.equals("")) {
                    JOptionPane.showConfirmDialog(mainFrame, "Bạn chưa chọn bạn để chat", "Lỗi", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                }
                else {
                    String friend_name = new String(selected_Friend_Name);
                    if (current_Peer_List == null) {
                        JOptionPane.showConfirmDialog(mainFrame, "Danh sách bạn chưa được hiển thị", "Lỗi", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                    }
                    else if (friend_name.equals(user_Name)) {
                        JOptionPane.showConfirmDialog(mainFrame, "Bạn không thể chat với chính mình", "Lỗi", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                    }
                    else {
                        for (int i = 0; i < current_Peer_List.size(); i++) {
                            if (friend_name.equals(current_Peer_List.get(i).getName())) {
                                try {
                                    Socket connClient = menuClient.requestChat(friend_name, current_Peer_List.get(i).getPort(), current_Peer_List.get(i).getHost());
                                    if (connClient != null) {
                                        isStop = true;
                                        new ChatGUI(user_Name, friend_name, connClient, peer_Port);
                                    }
                                    else {
                                        JOptionPane.showConfirmDialog(mainFrame, "Yêu cầu chat với bạn " + friend_name + " không được chấp nhận", "Lỗi", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE);
                                    }
                                    return;
                                }
                                catch (Exception exception) {
                                    exception.printStackTrace();
                                }
                            }
                        }
                        JOptionPane.showConfirmDialog(mainFrame, "Tài khoản đang offline hoặc không tồn tại", "Lỗi", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        mainFrame.getContentPane().add(btnChat);
        JButton btnExit = new JButton("Exit");
        btnExit.setBounds(205, 478, 113, 29);
        btnExit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int dialogResult = JOptionPane.showConfirmDialog(mainFrame, "Bạn thật sự có muốn thoát hay không", "Question", 
                JOptionPane.YES_NO_OPTION, 
                JOptionPane.QUESTION_MESSAGE);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    isStop = true;
                    menuClient.requestExit();
                    server.exit();
                    mainFrame.dispose();
                }
            }
        });
        mainFrame.getContentPane().add(btnExit);
    }

    public class UserItemRenderer extends JPanel implements ListCellRenderer<UserItem> {
        private JLabel iconLabel = new JLabel();
        private JLabel nameLabel = new JLabel();
        private JLabel icon1Label = new JLabel();

        public UserItemRenderer() {
            setLayout(new BorderLayout(5, 5));
            JPanel panelIcon = new JPanel();
            panelIcon.add(icon1Label);
            panelIcon.add(iconLabel);
            add(panelIcon, BorderLayout.WEST);
            add(nameLabel, BorderLayout.CENTER);
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends UserItem> list, UserItem item, int index, boolean isSelected, boolean cellHasFocus) {
            icon1Label.setIcon(new ImageIcon(workingDir + "/src/main/java/jar/images/online.jpg"));
            iconLabel.setIcon(new ImageIcon(item.getIconName()));
            nameLabel.setText(item.getName());

            // set Opaque to change background color of JLabel
            iconLabel.setOpaque(true);
            nameLabel.setOpaque(true);

            // when select item
            if (isSelected) {
                iconLabel.setBackground(list.getSelectionBackground());
                nameLabel.setBackground(list.getSelectionBackground());
                iconLabel.setForeground(list.getSelectionForeground());
                nameLabel.setForeground(list.getSelectionForeground());
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
            }
            else { // when don't select
                iconLabel.setBackground(list.getBackground());
                nameLabel.setBackground(list.getBackground());
                iconLabel.setForeground(list.getForeground());
                nameLabel.setForeground(list.getForeground());
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            return this;
        }
    }
    
    class UserItem {
        private String name;
        private String iconName;

        public UserItem(String name, String iconName) {
            this.name = name;
            this.iconName = iconName;
        }

        public String getName() {
            return name;
        }

        public String getIconName() {
            return iconName;
        }

        public void setName(String newName) {
            name = newName;
        }

        public void setIconName(String newIconName) {
            iconName = newIconName;
        }
    }

    private void displayFriendList(String message) {
        String temp = "";
        current_Peer_List = Decode.getAllAccount(message);
        for (int i = 0; i < current_Peer_List.size(); i++) {
            Peer peer = current_Peer_List.get(i);
            if (!peer.getName().equals(this.user_Name))
                temp = temp.concat(peer.getName() + "\n");
        }
        if (!temp.equals(peer_List)) {
            displayListUserItem();
            peer_List = new String(temp);
        }
    }

    private void updateFriendList() {
        while (!isStop) {
            String message = menuClient.getPeerList();
            displayFriendList(message);
        }
    }
    public static void main(String[] args )
    {
        EventQueue.invokeLater(new Runnable() {
                @Override
                public void run(){
                    MainGUI mainScreen = new MainGUI("0", 0, 0, "0", "0");
                    mainScreen.mainFrame.setVisible(true);
                }
            });
    }
}
