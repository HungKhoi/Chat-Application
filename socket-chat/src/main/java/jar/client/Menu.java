package jar.client;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;

import jar.protocol.Peer;
import jar.protocol.Tags;
import jar.protocol.Decode;
import jar.protocol.Encode;

public class Menu {
    private InetAddress server_IP_addr;
    private int server_Port = 8080;
    private String user_Name = "";
    private Socket client_Socket = null;
    private ObjectOutputStream sender;
    private ObjectInputStream listener;
    private Menu() {}

    public Menu(String server_ip, int server_port, String user_name) throws Exception{
        this.server_IP_addr = InetAddress.getByName(server_ip);
        this.server_Port = server_port;
        this.user_Name = user_name;
    }

    public String getPeerList() {
        String result_Message = "";
        try {
            SocketAddress addressServer = new InetSocketAddress(server_IP_addr, server_Port);
            client_Socket = new Socket();
            client_Socket.connect(addressServer);
            String msg = Encode.genOnlineMessage(user_Name);
            sender = new ObjectOutputStream(client_Socket.getOutputStream());
            sender.writeObject(msg);
            sender.flush();
            listener = new ObjectInputStream(client_Socket.getInputStream());
            result_Message = (String) listener.readObject();
            listener.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return result_Message;
    }

    public void requestExit() {
        try {
            SocketAddress addressServer = new InetSocketAddress(server_IP_addr, server_Port);
            client_Socket = new Socket();
            client_Socket.connect(addressServer);
            String msg = Encode.genOfflineMessage(user_Name);
            sender = new ObjectOutputStream(client_Socket.getOutputStream());
            sender.writeObject(msg);
            sender.flush();
            sender.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Socket requestChat(String friend_Name, int friend_Port, String friend_Host) {
        try {
            Socket connClient = new Socket(InetAddress.getByName(friend_Host), friend_Port);
            ObjectOutputStream sendRequestChat = new ObjectOutputStream(connClient.getOutputStream());
            sendRequestChat.writeObject(Encode.genChatRequest(user_Name));
            sendRequestChat.flush();
            ObjectInputStream receivedChat = new ObjectInputStream(connClient.getInputStream());
            String receivedMessage = (String) receivedChat.readObject();
            if (receivedMessage.equals(Tags.CHAT_ACCEPT_TAG)) {
                return connClient;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}