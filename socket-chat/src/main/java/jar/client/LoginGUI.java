package jar.client;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import jar.client.MainGUI;
import jar.protocol.Encode;
import jar.protocol.Tags;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import mdlaf.MaterialLookAndFeel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.io.File;
import java.util.Scanner;
/**
 * Login Screen for Client!
 *
 */
public class LoginGUI
{   
    static {
        try {
            UIManager.setLookAndFeel(new MaterialLookAndFeel());
            UIManager.put("Button.mouseHoverEnable", true);
            JFrame.setDefaultLookAndFeelDecorated(false);
            
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }
    private String workingDir = System.getProperty("user.dir");
    private JFrame loginFrame;
    private JTextField txtName, txtIP, txtPort;
    private JPasswordField txtPassword;
    public LoginGUI(){
        initUI();
    }
    private void initUI(){
        initFrame();
        initPictureBox();
        initLabel();
        initTextBox();
        initButton();
    }
    private void initFrame(){
        loginFrame = new JFrame();
        loginFrame.setTitle("Đăng nhập");
        loginFrame.setResizable(false);
		loginFrame.setSize(342, 459);
		loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        loginFrame.getContentPane().setLayout(null);
        loginFrame.setLocationRelativeTo(null);
        WindowAdapter adapter = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                int dialogResult = JOptionPane.showConfirmDialog(null, "Bạn thật sự có muốn thoát hay không", "Question", 
                    JOptionPane.YES_NO_OPTION, 
                    JOptionPane.QUESTION_MESSAGE);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        };
        loginFrame.addWindowListener(adapter);
    }
    private void initPictureBox(){
        JLabel pictureBox = new JLabel(new ImageIcon(workingDir + "/src/main/java/jar/images/chattingimage.jpg"));
        pictureBox.setBounds(50, 10, 250, 200);
        loginFrame.getContentPane().add(pictureBox);
    }
    private void initLabel(){
        JLabel accountLabel = new JLabel("Tài Khoản: ");
        accountLabel.setBounds(20, 240, 140, 14);
        loginFrame.getContentPane().add(accountLabel);
        JLabel passwordLabel = new JLabel("Mật Khẩu: ");
        passwordLabel.setBounds(20, 279, 140, 14);
        loginFrame.getContentPane().add(passwordLabel);
        JLabel ipLabel = new JLabel("IP: ");
        ipLabel.setBounds(20, 318, 60, 20);
        loginFrame.getContentPane().add(ipLabel);
        JLabel portLabel = new JLabel("Port: ");
        portLabel.setBounds(20, 357, 60, 20);
        loginFrame.getContentPane().add(portLabel);
    }
    private void initTextBox(){
        txtIP = new JTextField();
        txtIP.setColumns(10);
        txtIP.setBounds(110, 313, 200, 30);
        loginFrame.getContentPane().add(txtIP);
        txtName = new JTextField();
        txtName.setColumns(10);
        txtName.setBounds(110, 235, 200, 30);
        loginFrame.getContentPane().add(txtName);
        txtPort = new JTextField();
        txtPort.setColumns(10);
        txtPort.setBounds(110, 352, 200, 30);
        loginFrame.getContentPane().add(txtPort);
        txtPassword = new JPasswordField(20);
        txtPassword.setBounds(110, 274, 200, 30);
        loginFrame.getContentPane().add(txtPassword);
    }
    private void initButton(){
        JButton btnClose = new JButton("Đóng");
        btnClose.setBounds(235, 400, 90, 29);
        btnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int dialogResult = JOptionPane.showConfirmDialog(loginFrame, "Bạn thật sự có muốn thoát hay không", "Question", 
                    JOptionPane.YES_NO_OPTION, 
                    JOptionPane.QUESTION_MESSAGE);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });
        loginFrame.getContentPane().add(btnClose);
        JButton btnLogin = new JButton("Đăng nhập");
        btnLogin.setBounds(111, 400, 120, 29);
        btnLogin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (login(txtName.getText(), new String(txtPassword.getPassword()))) {
                    try {
                        String server_ip = txtIP.getText();
                        int server_port = Integer.parseInt(txtPort.getText());
                        String userName = txtName.getText();
                        Pattern checkName = Pattern.compile("[a-zA-Z_][a-zA-Z0-9_]*");
                        if (checkName.matcher(userName).matches()) {
                            Random random = new Random();
                            int peer_Port = 10000 + random.nextInt() % 1000;
                            InetAddress server_IP = InetAddress.getByName(server_ip);
                            Socket socketClient = new Socket(server_IP, server_port);
                            String message = Encode.genAccountRequest(userName, Integer.toString(peer_Port));
                            ObjectOutputStream sender = new ObjectOutputStream(socketClient.getOutputStream());
                            sender.writeObject(message);
                            sender.flush();
                            ObjectInputStream listener = new ObjectInputStream(socketClient.getInputStream());
                            message = (String) listener.readObject();
                            socketClient.close();
                            
                            if (message.equals(Tags.SESSION_DENY_TAG)) {
                                JOptionPane.showConfirmDialog(loginFrame, "Tên tài khoản đã tồn tại", "Lỗi", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                                return;
                            }
                            MainGUI mainGUI = new MainGUI(server_ip, server_port, peer_Port, userName, message);
                            mainGUI.show();
                            loginFrame.dispose();
                        }
                        else {
                            JOptionPane.showConfirmDialog(loginFrame, "Tên tài khoản không hợp lệ", "Lỗi", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    catch (Exception except) {
                        JOptionPane.showConfirmDialog(loginFrame, "Server đang offline hoặc không tồn tại", "Lỗi", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                        except.printStackTrace();
                    }
                }
                else {
                    JOptionPane.showConfirmDialog(loginFrame, "Tài khoản chưa được đăng ký hoặc nhập sai mật khẩu", "Lỗi", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        loginFrame.getContentPane().add(btnLogin);
        JButton btnclear = new JButton("Clear");
		btnclear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtIP.setText("");
				txtPort.setText("");
                txtName.setText("");
			}
		});
		btnclear.setBounds(15, 400, 90, 29);
		loginFrame.getContentPane().add(btnclear);
    }

    private boolean login(String username, String password) {
        File file = new File(workingDir + "/src/main/java/jar/database/account.txt");
        ArrayList<Account> acc_List = new ArrayList<Account>();
        try {
            Scanner scan = new Scanner(file);
            while(scan.hasNextLine()) {
                String str = scan.nextLine();
                int index = str.indexOf('|');
                Account new_Acc = new Account(str.substring(0, index), str.substring(index + 1, str.length()));
                acc_List.add(new_Acc);
            }
            scan.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        for (int i = 0; i < acc_List.size(); i++) {
            if ((acc_List.get(i).getName().equals(username)) && (acc_List.get(i).getPassword().equals(password))) {
                return true;
            }
        }
        return false;
    }

    class Account {
        private String name = "";
        private String password = "";
        public Account(String name, String password) {
            this.name = name;
            this.password = password;
        }

        public String getName() {
            return name;
        }

        public String getPassword() {
            return password;
        }

        public void setName(String newName) {
            name = newName;
        }

        public void setPassword(String newPassword) {
            password = newPassword;
        }
    }

    public static void main(String[] args )
    {
        EventQueue.invokeLater(new Runnable() {
                @Override
                public void run(){
                    LoginGUI loginScreen = new LoginGUI();
                    loginScreen.loginFrame.setVisible(true);
                }
            });
    }
}
