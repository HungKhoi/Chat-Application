package jar.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JOptionPane;

import jar.protocol.Tags;
import jar.protocol.Decode;

public class ServerClient {
    private ServerClient() { }
    private String userName = "";
    private int userPort = 1711611;
    private ServerSocket server_Client = null;
    private boolean isStop = false;
    public ServerClient(String userName, int userPort) {
        this.userName = userName;
        this.userPort = userPort;
        try {
            server_Client = new ServerSocket(this.userPort);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        (new WaitPeerConnect()).start();
    }

    public void exit() {
        try {
            isStop = true;
            server_Client.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    class WaitPeerConnect extends Thread{
        Socket connection;
        ObjectInputStream request;
        @Override
        public void run() {
            super.run();
            while (!isStop) {
                try {
                    connection = server_Client.accept();
                    request = new ObjectInputStream(connection.getInputStream());
                    String requestMessage = (String) request.readObject();
                    String name = Decode.getNameRequestChat(requestMessage);
                    int dialogResult = JOptionPane.showConfirmDialog(null, "Bạn có muốn chat với " + name + " không ?", "Question", 
                        JOptionPane.YES_NO_OPTION, 
                        JOptionPane.QUESTION_MESSAGE);
                    ObjectOutputStream sender = new ObjectOutputStream(connection.getOutputStream());
                    if (dialogResult == JOptionPane.YES_OPTION) {
                        sender.writeObject(Tags.CHAT_ACCEPT_TAG);
                        new ChatGUI(userName, name, connection, userPort);
                    }
                    else {
                        sender.writeObject(Tags.CHAT_DENY_TAG);
                    }
                    sender.flush();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
				server_Client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
    }
}